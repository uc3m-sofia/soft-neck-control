#include "Cia402device.h"
#include "CiA301CommPort.h"
#include "SocketCanPort.h"
#include <iostream>

using namespace std;

long rpy2tendon(vector<double> rpy, vector<double>& tl)
{

    tl[0] = rpy[1]/1.5;
    tl[1] = ( - (rpy[1] / 3) - (rpy[0] / 1.732) );
    tl[2] = ( (rpy[0] / 1.732) - (rpy[1] / 3) );

    cout<<"tl[0]: "<<tl[0]<<", "<<"tl[1]: "<<tl[1]<<", "<<"tl[2]: "<<tl[2]<<endl;

    return 0;
}


int main ()
{
    //--Can port communications--
    SocketCanPort pm31("can0");
    SocketCanPort pm32("can0");
    SocketCanPort pm33("can0");

    CiA402SetupData sd31(2048,24,0.001, 0.144,20);
    CiA402SetupData sd32(2048,24,0.001, 0.144,20);
    CiA402SetupData sd33(2048,24,0.001, 0.144,20);

    CiA402Device m1 (1, &pm31, &sd31);
    CiA402Device m2 (2, &pm32, &sd32);
    CiA402Device m3 (3, &pm33, &sd33);


    //    Motor setup
    m1.Reset();
    m1.SwitchOn();

    m2.Reset();
    m2.SwitchOn();

    m3.Reset();
    m3.SwitchOn();
    //     m33.DisablePDOs();


    //set velocity and aceleration (rads/s)
    m1.SetupPositionMode();
    m2.SetupPositionMode();
    m3.SetupPositionMode();

    double pos1, pos2, pos3;
    double vel1, vel2, vel3;

    double rm=0.0075; //winch radius
vector<double> rpy = {0.0,0.04,0};
vector<double> tl(3);


rpy2tendon(rpy,tl);

    // position  [rads]
    m1.SetPosition(tl[0]/rm);
    m2.SetPosition(tl[1]/rm);
    m3.SetPosition(tl[2]/rm);

    for (int i=0;i<10;i++){

        pos1 = m1.GetPosition();
        pos2 = m2.GetPosition();
        pos3 = m3.GetPosition();

        vel1 = m1.GetVelocity();
        vel2 = m2.GetVelocity();
        vel3 = m3.GetVelocity();

        cout<<"pos1: "<<pos1<<", "<<"pos2: "<<pos2<<", "<<"pos3: "<<pos3<<endl;
        cout<<"vel1: "<<vel1<<", "<<"vel2: "<<vel2<<", "<<"vel3: "<<vel3<<endl;
        sleep(1);
    }
    m1.SetPosition(0);
    m2.SetPosition(0);
    m3.SetPosition(0);
    sleep(3);


}
